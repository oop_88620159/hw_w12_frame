/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w12_frame;

/**
 *
 * @author acer
 */
public class Triangle extends Shape {

    private double base;
    private double hei;

    public Triangle(double base, double hei) {
        super("Triangle");
        this.base = base;
        this.hei = hei;

    }


    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHei() {
        return hei;
    }

    public void setHei(double hei) {
        this.hei = hei;
    }

    @Override
    public double calArea() {
        return 0.5 * base * hei;
    }

    @Override
    public double calPerimeter() {
        return (hei * 2) + base;
    }
}
