/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w12_frame;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class TriangleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblbase = new JLabel("base", JLabel.TRAILING);
        lblbase.setSize(50, 20);
        lblbase.setLocation(5, 5);
        lblbase.setBackground(Color.WHITE);
        lblbase.setOpaque(true);
        frame.add(lblbase);
        
        JLabel lblHei = new JLabel("height", JLabel.TRAILING);
        lblHei.setSize(50, 20);
        lblHei.setLocation(5,30);
        lblHei.setBackground(Color.WHITE);
        lblHei.setOpaque(true);
        frame.add(lblHei );
        
        
        final JTextField txtbase= new JTextField();
        txtbase.setSize(50, 20);
        txtbase.setLocation(60, 5);
        frame.add(txtbase);
        
        final JTextField txtHei = new JTextField();
        txtHei.setSize(50, 20);
        txtHei.setLocation(60, 30);
        frame.add(txtHei);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120,15);
        frame.add(btnCalculate);

        
        final JLabel lblResult1 = new JLabel("Base= ?? Height = ??");
        lblResult1.setHorizontalAlignment(JLabel.CENTER);
        lblResult1.setSize(300, 25);
        lblResult1.setLocation(0, 80);
        lblResult1.setBackground(Color.MAGENTA);
        lblResult1.setOpaque(true);
        frame.add(lblResult1);
        
        final JLabel lblResult2 = new JLabel("area = ?? perimeter = ??");
        lblResult2.setHorizontalAlignment(JLabel.CENTER);
        lblResult2.setSize(300, 25);
        lblResult2.setLocation(0, 110);
        lblResult2.setBackground(Color.MAGENTA);
        lblResult2.setOpaque(true);
        frame.add(lblResult2);
        
        
         btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strbase = txtbase.getText();
                    String strHei = txtHei.getText();
                    double base = Double.parseDouble(strbase);
                    double hei = Double.parseDouble(strHei);
                    Triangle triangle = new Triangle(base,hei);
                    lblResult1.setText("Base = " + triangle.getBase() +
                            " Height = " + triangle.getHei());
                    lblResult2.setText(" area = " + String.format("%.2f",triangle.calArea())
                            + " perimeter = " + String.format("%.2f",triangle.calPerimeter()));
                    
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error Plese Input Number: ",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtbase.setText("");
                    txtHei.setText("");
                    txtbase.requestFocus();
                    txtHei.requestFocus();
                }
            }

        });

        

        
        frame.setVisible(true);
    }
}


