/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w12_frame;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class SquareFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Square");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblsize = new JLabel("size", JLabel.TRAILING);
        lblsize.setSize(50, 20);
        lblsize.setLocation(5, 5);
        lblsize.setBackground(Color.WHITE);
        lblsize.setOpaque(true);
        frame.add(lblsize);

        final JTextField txtsize = new JTextField();
        txtsize.setSize(50, 20);
        txtsize.setLocation(60, 5);
        frame.add(txtsize);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Square Size = ?? area = ?? perimeter = ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strsize = txtsize.getText();
                    double size = Double.parseDouble(strsize);
                    Square square = new Square(size);
                    lblResult.setText("Square size = " + square.getSize()
                            + " area = " + String.format("%.2f", square.calArea())
                            + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error Plese Input Number: ",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtsize.setText("");
                    txtsize.requestFocus();

                }
            }

        });

        frame.setVisible(true);
    }
}

