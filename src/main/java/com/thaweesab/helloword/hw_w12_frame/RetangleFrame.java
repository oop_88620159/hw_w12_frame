/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w12_frame;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class RetangleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Retangle");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblWidth = new JLabel("width", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        frame.add(lblWidth);
        
        JLabel lblHeight = new JLabel("height", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5,30);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        frame.add(lblHeight );
        
        
        final JTextField txtWidth= new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        frame.add(txtWidth);
        
        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 30);
        frame.add(txtHeight);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120,15);
        frame.add(btnCalculate);

        
        final JLabel lblResult1 = new JLabel("Width = ?? Height = ??");
        lblResult1.setHorizontalAlignment(JLabel.CENTER);
        lblResult1.setSize(300, 25);
        lblResult1.setLocation(0, 80);
        lblResult1.setBackground(Color.MAGENTA);
        lblResult1.setOpaque(true);
        frame.add(lblResult1);
        
        final JLabel lblResult2 = new JLabel("area = ?? perimeter = ??");
        lblResult2.setHorizontalAlignment(JLabel.CENTER);
        lblResult2.setSize(300, 25);
        lblResult2.setLocation(0, 110);
        lblResult2.setBackground(Color.MAGENTA);
        lblResult2.setOpaque(true);
        frame.add(lblResult2);
        
        
         btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    String strHeight = txtHeight.getText();
                    double width = Double.parseDouble(strWidth);
                    double height = Double.parseDouble(strHeight);
                    Retangle retangle = new Retangle(width,height);
                    lblResult1.setText("Width = " + retangle.getWidth()+
                            " Height = " + retangle.getHeight());
                    lblResult2.setText(" area = " + String.format("%.2f", retangle.calArea())
                            + " perimeter = " + String.format("%.2f", retangle.calPerimeter()));
                    
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error Plese Input Number: ",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtHeight.setText("");
                    txtWidth.requestFocus();
                    txtHeight.requestFocus();
                }
            }

        });

        

        
        frame.setVisible(true);
    }
}


