/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w12_frame;

/**
 *
 * @author acer
 */
public class Square extends Shape {

    private double size;

    public Square(double size) {
        super("Square");
        this.size = size;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
    
    @Override
    public double calArea() {
        return size * size;
    }

    @Override
    public double calPerimeter() {
        return size * 4;
    }

}
