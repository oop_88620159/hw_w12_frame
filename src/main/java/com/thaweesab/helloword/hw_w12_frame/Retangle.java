/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w12_frame;

/**
 *
 * @author acer
 */
public class Retangle extends Shape {

    private double width;
    private double height;

    public Retangle(double width, double height) {
        super("Retangle");
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double calArea() {
        return width * height;
    }

    @Override
    public double calPerimeter() {
        return (width + height) * 2;
    }

}
